#!/usr/bin/env python

__author__ = 'Elric Hindy'
__version__ = '1.0.1'
__date__ = 'March 30 2016'

# Basic imports
import sys
import logging
import logging.config
import threading
import argparse
import time
import os
import platform
from decimal import *

# from Queue import *

# python 3 and 2 compatibility import and global variables
if (sys.version_info > (3, 0)):
    # Python 3 imports  in this block
    from configparser import *

    SafeConfigParser = ConfigParser
    pythonversion = 3
else:
    # Python 2 imports in this block
    from ConfigParser import *

    pythonversion = 2

# Phidget specific imports
from Phidgets.PhidgetException import PhidgetException
from Phidgets.Devices.InterfaceKit import InterfaceKit
from Phidgets import Manager
from Phidgets.Phidget import PhidgetLogLevel
from argparse import RawTextHelpFormatter
from datetime import datetime
from usbreset import *


# Event Handler Callback Functions
def interfaceKitAttached(e):
    """Interface Kit Attached Handler"""
    logger = logging.getLogger("file")
    attached = e.device
    logger.info("InterfaceKit %i Attached!" % (attached.getSerialNum()))


def interfaceKitDetached(e):
    """InterfaceKit Detach Handler"""
    logger = logging.getLogger("file")
    detached = e.device
    logger.info("InterfaceKit %i Detached!" % (detached.getSerialNum()))


def interfaceKitError(e):
    """InterfaceKit Error Handler"""
    logger = logging.getLogger("file")
    try:
        source = e.device
        logger.error("InterfaceKit %i: Phidget Error %i: %s" % (source.getSerialNum(), e.eCode, e.description))
    except PhidgetException as e:
        logger.error("Phidget Exception {0}: {1}".format(e.code, e.details), exc_info=True)


def interfaceKitOutputChanged(e):
    """Interface Kit Ouput Change Handler"""
    logger = logging.getLogger("file")
    source = e.device
    logger.debug("InterfaceKit %i: Output %i: %s" % (source.getSerialNum(), e.index, e.state))


def open_InterfaceKit(serial=-1, remoteip=False, remoteport=5001, phidgetdebug=False):
    """
    Creates an interfacekit object
    :param serial: Serial of Interface Kit to Connect To
    :param remoteip: Remote IP of Phidget server to connect to or False to connect to Local Deivce
    :param remoteport: Remote Port of Phidget Server, IOgnored if local
    :param phidgetdebug: Whether to enable Phidget debug logs
    :return: Interface Kit Obj if Successful or exit programs if failure
    """
    logger = logging.getLogger("root")
    if remoteip and isinstance(remoteip, str):
        remoteip = remoteip.encode('utf-8')

    try:
        interfaceKit = ThreadedInterfaceKit()
    except RuntimeError as e:
        logger.error("Runtime Exception: %s" % e.message)
        exit(1)

    try:
        # logging example, uncomment to generate a log file
        if phidgetdebug:
            # if pythonversion == 2:
            #     phidgetLogFile = 'phidget.log'
            # elif pythonversion == 3:
            #     phidgetLogFile = b'phidget.log'
            phidgetLogFile = b'phidget.log'

            interfaceKit.enableLogging(PhidgetLogLevel.PHIDGET_LOG_VERBOSE, phidgetLogFile)
            logger.info("Debug Phidget Device Logging enabled with {0}".format(phidgetLogFile))

        # create event handlers
        interfaceKit.setOnErrorhandler(interfaceKitError)

        # actions on phidget device being attached or detached
        interfaceKit.setOnAttachHandler(interfaceKitAttached)
        interfaceKit.setOnDetachHandler(interfaceKitDetached)

        # TODO: why is this here
        # Action on output change
        if phidgetdebug:
            interfaceKit.setOnOutputChangeHandler(interfaceKitOutputChanged)
            logger.info("Debug Phidget Output Change Handler Enabled")

    except PhidgetException as e:
        logger.error("Phidget Exception %i: %s" % (e.code, e.details))
        logger.info("Exiting....")
        exit(1)

    logger.debug("Opening phidget object....")

    try:
        if remoteip:
            logger.info("Opening remote phidget SN# {0} at {1} on port {2}".format(serial, remoteip, remoteport))
            interfaceKit.openRemoteIP(remoteip, remoteport, serial=serial, password=b'')
        else:
            logger.info("Opening local phidget device")
            interfaceKit.openPhidget(serial=serial)
    except PhidgetException as e:
        logger.error("Phidget Exception %i: %s" % (e.code, e.details))
        exit(1)

    logger.info("Waiting for InterfaceKit #{0} to attach....".format(serial))
    try:
        interfaceKit.waitForAttach(10000)
    except PhidgetException as e:
        logger.error("Phidget Exception %i: %s" % (e.code, e.details))
    else:
        logger.debug("Interface Kit Attached!")
        return interfaceKit

    # if failed to connect try resetting device usb
    if not remoteip:
        try:
            logger.info("Resetting phidget USB port..")
            usbobj = UsbReset('Phidget', log=True)
            usbobj.resetusbdev()
        except InvalidDeviceError as e:
            logger.error("Error resetting USB port: Exception {0}".format(e))

            try:
                interfaceKit.closePhidget()
            except PhidgetException as e:
                logger.error("Phidget Exception %i: %s" % (e.code, e.details))
            finally:
                logger.info("Exiting....")
                print("Error connecting to phidget device, check logs for more information")
                exit(1)
        except OSSupportError as e:
            logger.error("Unsupported OS for resetting usb: {0}".format(e))
            logger.error("Exiting program as can not connect to Phidget Device..")
            print("Error connecting to phidget device, check logs for more information")
            exit(1)
        else:
            logger.info("USB Phidget reset successfull")

        # try to attach after succesfull reset
        logger.info("Waiting for InterfaceKit to attach....")
        try:
            interfaceKit.waitForAttach(10000)
        except PhidgetException as e:
            logger.error("Phidget Exception %i: %s" % (e.code, e.details))
            try:
                interfaceKit.closePhidget()
            except PhidgetException as e:
                logger.error("Phidget Exception %i: %s" % (e.code, e.details))
            finally:
                logger.info("Exiting....")
                exit(1)
        else:
            logger.info("Interface Kit Attached!")
            return interfaceKit
    else:
        msg = "Failed to connect to phidget device {0}".format(serial)
        raise IOError(msg)


def close_InterfaceKit(interfaceKit):
    """
    Function to safely close a the InterfaceKit
    :param interfaceKit: Interface kit to close
    :return: True if successfull, False if failure to close device.
    """
    logger = logging.getLogger("root")
    try:
        logger.debug("Closing Phidget device...")
        interfaceKit.closePhidget()
    except PhidgetException as e:
        logger.error("Phidget Exception %i: %s" % (e.code, e.details))
        return False
    return True


class Font(object):
    """Fonts class for displaying colours and other font styles"""

    if 'linux' in platform.system().lower():
        PURPLE = '\033[95m'
        BOLD = '\033[1m'
        END = '\033[0m'
        BLACK = '\033[0;30m'
        DARK_GREY = '\033[1;30m'
        RED = '\033[1;31m'
        BRIGHT_RED = '\033[1;31m'
        GREEN = '\033[32m'
        BRIGHT_GREEN = '\033[1;32m'
        BROWN = '\033[0;33m'
        YELLOW = '\033[1;33m'
        BLUE = '\033[0;34;47m'
        BRIGHT_BLUE = '\033[1;34m'
        MAGENTA = '\033[0;35m'
        BRIGHT_MAGENTA = '\033[1;35m'
        CYAN = '\033[36m'
        BRIGHT_CYAN = '\033[1,36m'
        LIGHT_GREY = '\033[37m'
        WHITE = '\033[37m'
        UNDERLINE = '\033[4m'
        BRIGHT_BOLD_UNDERLINE = '\033[1;4m'
    else:
        PURPLE = ''
        BOLD = ''
        END = ''
        BLACK = ''
        DARK_GREY = ''
        RED = ''
        BRIGHT_RED = ''
        GREEN = ''
        BRIGHT_GREEN = ''
        BROWN = ''
        YELLOW = ''
        BLUE = ''
        BRIGHT_BLUE = ''
        MAGENTA = ''
        BRIGHT_MAGENTA = ''
        CYAN = ''
        BRIGHT_CYAN = ''
        LIGHT_GREY = ''
        WHITE = ''
        UNDERLINE = ''
        BRIGHT_BOLD_UNDERLINE = ''


def convertSeconds(seconds):
    """
    Converts seconds to hours minutes and seconds
    :param seconds: Seconds to convert
    :return: tuple of (Hours, Minutes, Seconds)
    """
    neg = False
    if seconds < 0:
        seconds = abs(seconds)
        neg = True
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    if neg:
        h = -1 * h
    return h, m, s


class CountdownFormatter(object):
    def __init__(self, name, counter, total, cycle, cycles):
        self.name = name
        self.cycle = cycle
        self.cyclename = "NotSet"
        self.cycles = cycles
        self.precision = Decimal("1.0")
        self._cycletime = Decimal("0.0") / self.precision
        self.counter = Decimal(str(counter)) / self.precision
        self.total = Decimal(str(total)) / self.precision
        self.totaltime = Decimal(str(total))

    @property
    def cycletime(self):
        return self._cycletime * self.precision

    @cycletime.setter
    def cycletime(self, value):
        self._cycletime = Decimal(str(value)) / self.precision

    def set_precision(self, value):
        value = Decimal(str(value))
        if value == self.precision:
            return None
        self._cycletime *= self.precision
        self.counter *= self.precision
        self.total *= self.precision
        self.precision = value
        self._cycletime /= self.precision
        self.counter /= self.precision
        self.total /= self.precision

    def get_string(self):
        return self.__str__()

    def __str__(self):
        if self.total < 0:
            th, tm, ts = convertSeconds(self.totaltime)
            return "{0} finished {1} cycle(s) in {2:0>3.0f}h {3:0>2.0f}m {4:0>2.2f}s".format(self.name, self.cycles, th,
                                                                                             tm, ts)
        ch, cm, cs = convertSeconds(self._cycletime * self.precision)
        th, tm, ts = convertSeconds(self.total * self.precision)
        str_out = "{0}:{7:>3} {8}/{9} {1:0>3.0f}h {2:0>2.0f}m {3:0>2.2f}s, Rem: {4:0>3.0f}h {5:0>2.0f}m {6:0>2.2f}s".format(
            self.name, ch, cm, cs, th, tm, ts, self.cyclename, self.cycle, self.cycles)
        return str_out


class CountdownConsole(threading.Thread):
    """Threading class for displaying cycle and finish times in a separate thread to keep core timing accurate"""

    def __init__(self, times, precision=0.1):
        """
        Threading class for displaying cycle and finish times in a separate thread to keep core timing accurate
        :param name: Thread Name
        :param counter: Time to Count down from
        :param total: Remainin time left
        :param precision: Count down precision in seconds
        """
        threading.Thread.__init__(self)
        self._precision = Decimal(str(precision))
        self.display = times
        for d in self.display:
            d.set_precision(precision)
        self._stop = threading.Event()
        self.logger = logging.getLogger("file")

    def run(self):
        self.printCounter()

    def stop(self):
        self._stop.set()
        # sys.stdout.write("\r{0:90}\r".format(""))
        sys.stdout.flush()
        self.logger.debug("Exiting {0} Thread, please wait...".format(self.name))
        time.sleep(0.1)

    def isStopped(self):
        return self._stop.isSet()

    def printCounter(self):
        """Displays the current count down for the thread on the screen"""

        while not self.isStopped():
            out_string = "\r"
            for i, d in enumerate(self.display):
                out_string += d.get_string()
                if i < len(self.display) - 1 and len(self.display) > 1:
                    out_string += " | "
            # out_string += "\r"
            sys.stdout.write(out_string)

            for d in self.display:
                if d.cycletime > 0:
                    d.cycletime -= self._precision
                d.total -= 1
            time.sleep(self._precision)
            sys.stdout.flush()
        self.logger.debug("Thread {0} Completed Successfully".format(self.name))


class ThreadedInterfaceKit(InterfaceKit, object):
    """
    used for locking interface kit if accessed by multiple threads to not cause access issues
    """

    def __init__(self):
        self.logger = logging.getLogger('root')
        self.lock = threading.Lock()
        super(ThreadedInterfaceKit, self).__init__()
        self.busy = False

    def getOutputCount(self):
        # self.logger.info("Acquiring Lock")
        self.lock.acquire()
        # self.logger.info("Lock Acquired")
        result = super(ThreadedInterfaceKit, self).getOutputCount()
        self.lock.release()
        # self.logger.info("Lock Released")
        return result

    def getOutputState(self, index):
        # while self.busy:
        #     time.sleep(0.1)
        #     self.busy = True
        # self.logger.info("Acquiring Lock")
        self.lock.acquire()
        # self.logger.info("Lock Acquired")
        result = super(ThreadedInterfaceKit, self).getOutputState(index)
        self.lock.release()
        # self.logger.info("Lock Released")
        # self.busy = False
        return result

    def setOutputState(self, index, state):
        # while self.busy:
        #     time.sleep(0.1)
        #     self.busy = True
        # self.logger.info("Acquiring Lock")
        self.lock.acquire()
        # self.logger.info("Lock Acquired")
        result = super(ThreadedInterfaceKit, self).setOutputState(index, state)
        self.lock.release()
        # self.logger.info("Lock Released")
        # self.busy = False
        return result


def getConfig(filename):
    """
    Reads the config file and returns the config object
    :param filename: config file name
    :return: ConfigParser Object
    """
    # TODO: if no config create default
    config = SafeConfigParser()
    if not os.path.exists(filename):
        open(filename, 'a').close()
    config.read(filename)

    return config


def create_device_configurations(filename, devices, timingdata):
    """
    Reads the config file and returns the config object for each device
    :param filename: config file name
    :return: ConfigParser Object
    """
    config = getConfig(filename)
    device_list = []
    for i, device in enumerate(devices):
        device_list.append(DeviceConfig(device, config.items(device), timingdata[i]))

    return device_list


class DeviceConfig(object):
    def __init__(self, name, attributes, timingdata):
        self.name = name
        self.timingdata = timingdata
        self.port_list = []
        self.con_list = []
        self.ports = []
        for key, value in attributes:
            if 'port' in key:
                key_code = key.replace('port','')
                self.port_list.append([key_code, int(value)])
                value = int(value)
            elif 'connection' in key:
                key_code = key.replace('connection', '')
                self.con_list.append([key_code, value])

            setattr(self, key, value)
        for i in self.port_list:
            found = False
            for j in self.con_list:
                if j[0] == i[0]:
                    found = True
                    self.ports.append([i[0], (i[1], j[1])])
            if not found:
                self.ports.append([i[0], (i[1], 'NC')])

        # self.ignitionport = int(self.ignitionport)
        # self.powerport = int(self.powerport)
        self.ikitserial = int(self.ikitserial)


def printStates(devices):
    """
    Prints the Current State of the ports based on the config of the device
    :param devices: Lost of controller objects to print stat of
    :return:
    """

    logger = logging.getLogger('file')

    # Requires short wait time for devices to finish current operation
    time.sleep(0.2)

    # Print the heading
    logger.debug("Printing Device States")
    print(
        '\n' + Font.UNDERLINE + "{0:^10} | {1:>8} : {2:^5} : {3:<5} | {4:<6}".format("Device", "Switch", "Port",
                                                                                     "Config",
                                                                                     "State") + Font.END)

    # Prints state of devices
    for device in devices:
        pwrstate, ignstate = device.getState()
        powerstatusstring = "{0: ^10} | {1: >8} : {2:^5} : {3:^6} | {4: ^14}".format(device.devicename, "Power",
                                                                                     device.pwrPort, device.pwrCon,
                                                                                     pwrstate) + Font.END
        ignitionstatusstring = Font.UNDERLINE + "{0: ^10} | {1: >8} : {2:^5} : {3:^6} | {4: ^14}".format("", "Ignition",
                                                                                                         device.ignPort,
                                                                                                         device.ignCon,
                                                                                                         ignstate) + Font.END
        print(powerstatusstring)
        print(ignitionstatusstring)
    print('\n')


def configure_logging(rlog_level='DEBUG', flog_level='DEBUG', clog_level='DEBUG', debug=False, date=False,
                      log_config="phidgetlogger.config"):
    """
    Configures the logging for the devices
    :param rlog_level: Root Log Level
    :param flog_level: File Log Level
    :param clog_level: Console Log Level
    :param debug: If debug logs enabled
    :param date: Wheher to use the Date File handler or normal handler
    :param log_config: The Logging configuration file to load (added to support unnittest logging)
    :return: None
    """
    # configure logging and levels
    # LogConfig.root_level = rlog_level
    # LogConfig.console_level = clog_level
    # LogConfig.file_level = flog_level
    # LogConfig.debug_enabled = debug
    logging.config.fileConfig(log_config, disable_existing_loggers=False)
    rootlogger = logging.getLogger("root")
    fileLogger = logging.getLogger("file")
    datefileLogger = logging.getLogger("datefile")
    consoleLogger = logging.getLogger("console")
    debugLogger = logging.getLogger("debug")
    if date:
        hdlr = datefileLogger.handlers[0]
        fileLogger.addHandler(hdlr)
        rootlogger.addHandler(hdlr)
    if not debug:
        debugLogger.disabled = True
    else:
        rootlogger.info("Debug logging Enabled")
    rootlogger.setLevel(logging.getLevelName(rlog_level))
    fileLogger.setLevel(logging.getLevelName(flog_level))
    datefileLogger.setLevel(logging.getLevelName(flog_level))
    consoleLogger.setLevel(logging.getLevelName(clog_level))


def load_script(filename):
    """
    Loads a script file to run through
    :param filename: script filename to open
    :return: True is Sucessful, False if error
    """
    root_logger = logging.getLogger('root')
    console_logger = logging.getLogger('console')
    with open(filename, "r") as f:
        timingdata = f.readlines()

    # strip data in to useful values
    cycles = 0
    totaltime = 0  # total run time of script
    try:
        for i, line in enumerate(timingdata):
            if line == '\n':
                continue
            timingdata[i] = line.strip().split(',')
            for x, t in enumerate(timingdata[i]):
                timingdata[i][x] = float(t)
                totaltime += float(t)
            cycles += 1
    except ValueError as e:
        root_logger.error("Script contains invalid data on line 0, column {1} = {2} is not an int".format(i, x, t))
        root_logger.error("{0}".format(e))
        return False

    # calculate script run time
    h, m, s = convertSeconds(totaltime)
    console_logger.info("Running from scipt: {0}".format(filename))
    console_logger.info("%i Cycles; Total run time ~%dh:%02dm:%02ds" % (cycles, h, m, s))
    starttime = int(time.time())

    return TimingData(starttime, totaltime, cycles, timingdata)


def calc_cycles(cycles, time_ons, time_offs, devices):
    """
    Calulates cycle time and generate timing object
    :param cycles: Number of cycles
    :param time_on: Time devices is on
    :param time_off: Time device is off
    :return: TimingData Object
    """
    console_logger = logging.getLogger('console')
    timings = []
    for i in range(len(devices)):
        totaltime = float(cycles[i] * (time_ons[i] + time_offs[i]))
        h, m, s = convertSeconds(totaltime)
        #console_logger.info("Running multiple cycles for {0}".format(devices[i]))
        console_logger.info("{0} Cycles; Total run time for {1} ~{2:0>3.0f}h {3:0>2.0f}m {4:0>2.2f}s".format(cycles[i], devices[i], h, m, s))
        starttime = int(time.time())

        # generate timing data
        timingdata = []
        for j in range(cycles[i]):
            timingdata.append([time_ons[i], time_offs[i]])

        timings.append(TimingData(starttime, totaltime, cycles[i], timingdata))

    return timings


class TimingData(object):
    """
    Timing Data Object
    """

    def __init__(self, starttime=0, totaltime=0, cycles=0, timingdata=[], switchpower=False, switchignition=True,
                 startvalue='keep', endvalue='keep', switch='tog'):
        """
        Initialisation of Timing Data Object
        :param starttime: Start time of run
        :param totaltime: Total time of run
        :param cycles: Number of cycles
        :param timingdata: List of [[on time, off time],[...,...]]
        """
        self.starttime = starttime
        self.totaltime = totaltime
        self.cycles = cycles
        self.timingdata = timingdata
        self.switchpower = switchpower
        self.switchignition = switchignition
        self.startvalue = startvalue
        self.endvalue = endvalue
        self.switch = switch
        self._currentcycle = 0
        self.display = CountdownFormatter("NotSet", self.totaltime, self.totaltime, self.cycles, self.cycles)
        self._name = "NotSet"

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        self.display.name = value

    def getnext(self):
        try:
            ton = float(self.timingdata[self._currentcycle][0])
            toff = float(self.timingdata[self._currentcycle][1])
        except IndexError as e:
            print("{3} Tried to get {0} in {1}".format(self._currentcycle, len(self.timingdata), self.name))
        self._currentcycle += 1
        return ton, toff

    def reset(self):
        self.cycle = self.cycles
        self.display = CountdownFormatter(self.name, self.counter, self.total, self.cycle, self.cycles)


class Device(object):
    """"Class for controlling each device configured in the Phidget.config file"""

    def __init__(self, devicename, pwrport, pwrcon, ignport, ignconn, timingdata, iface, devconfig=None):
        """
        Initialisation of Controller Class
        :param device: Devices Name
        :param config: ConfigParser Config obj
        :param iface: Interface Kit connected to device
        """
        self.file_logger = logging.getLogger("file")
        self.root_logger = logging.getLogger("root")

        self.devicename = devconfig.name
        self.pwrCon = devconfig.powerconnection
        self.pwrPort = devconfig.powerport
        self.ignPort = devconfig.ignitionport
        self.ignCon = devconfig.ignitionconnection
        self.timing = devconfig.timingdata
        self.timing.name = self.devicename
        self.interfaceKit = iface

        if devconfig:
            for key, value in devconfig.ports:

                setattr(self, key, value)

        # Set up whether the port is Normally Connected (NC) or Normally Open(NO) based off the config file
        outputcount = self.interfaceKit.getOutputCount()
        if self.ignPort > outputcount - 1:
            self.root_logger.error(
                "Ignition Port on device {0} is {1}, this is greater than ports available 0->{2}".format(
                    self.devicename,
                    self.pwrPort,
                    outputcount - 1))
            raise IOError("Not enough ports for {0}".format(self.devicename))
        elif self.pwrPort > outputcount - 1:
            self.root_logger.error(
                "Power Port on device {0} is {1}, this is greater than ports available 0->{2}".format(
                    self.devicename,
                    self.pwrPort,
                    outputcount - 1))
            raise IOError("Not enough ports for {0}".format(self.devicename))

        self.i_on = False if self.ignCon == 'NC' else True
        self.p_on = False if self.pwrCon == 'NC' else True

        # if self.ignCon == 'NC':
        #     self.i_on = False
        #     self.i_off = True
        # elif self.ignCon == 'NO':
        #     self.i_on = True
        #     self.i_off = False
        #
        # if self.pwrCon == 'NC':
        #     self.p_on = False
        #     self.p_off = True
        # elif self.pwrCon == 'NO':
        #     self.p_on = True
        #     self.p_off = False

    def changeOutput(self, argP, argI, state):
        """
        Controls changing of the devices output state
        :param argP: True to change Power
        :param argI: True to Chaneg Ignition
        :param state: state to change device 'on', 'off', 'tog'
        :return: True if success
        """
        self.file_logger.info(
            "Changing Output state of {0} with P = {1}, I = {2} & State = {3}".format(self.devicename, argP, argI,
                                                                                      state))

        # get current states:
        pwrstate, ignstate = self.getState(raw_values=True)

        # Power port control
        if argP:
            if ((pwrstate == 1) & (state == 'on')) | ((pwrstate == 0) & (state == 'off')):
                self.file_logger.info("{0}: Power State already set to {1}".format(self.devicename, state))
            else:
                self.file_logger.debug("Switching Power {0}".format(state))
                if state == 'on':
                    self.interfaceKit.setOutputState(self.pwrPort, self.p_on)
                elif state == 'off':
                    self.interfaceKit.setOutputState(self.pwrPort, not self.p_on)
                elif state == 'tog':
                    self.interfaceKit.setOutputState(self.pwrPort, not (self.interfaceKit.getOutputState(self.pwrPort)))

        # Igintion Port Control
        if argI:
            if ((ignstate == 1) & (state == 'on')) | ((ignstate == 0) & (state == 'off')):
                self.file_logger.info("{0}: Ignition State already set to {1}".format(self.devicename, state))
            else:
                self.file_logger.debug("Switching Ignition {0}".format(state))
                if state == 'on':
                    self.interfaceKit.setOutputState(self.ignPort, self.i_on)
                elif state == 'off':
                    self.interfaceKit.setOutputState(self.ignPort, not self.i_on)
                elif state == 'tog':
                    self.interfaceKit.setOutputState(self.ignPort, not (self.interfaceKit.getOutputState(self.ignPort)))
        return True

    def getState(self, raw_values=False):
        """
        Returns the current state of the system,
            Default return colour coded formatted string for ON and OFF
        :param raw_values: True to return True or False for On and Off respectively rather than formatted string
        :return: String or Boolean values for current state of device
        """
        pwrstate = bool(self.interfaceKit.getOutputState(self.pwrPort))
        if self.pwrCon == 'NC':
            pwrstate = not pwrstate
        if pwrstate:
            pwr_state_s = Font.GREEN + "ON " + Font.END
        else:
            pwr_state_s = Font.RED + "OFF" + Font.END

        ignstate = bool(self.interfaceKit.getOutputState(self.ignPort))
        if self.ignCon == 'NC':
            ignstate = not ignstate
        if ignstate:
            ign_state_s = Font.GREEN + "ON " + Font.END
        else:
            ign_state_s = Font.RED + "OFF" + Font.END

        if raw_values:
            return pwrstate, ignstate
        else:
            return pwr_state_s, ign_state_s

    def run(self):
        # t = threading.Thread(target=self.run_cycles)
        # t.daemon = True
        # t.start()
        self.run_cycles()

    def run_cycles(self):
        """
        Run cycles using a timing data object
        :param device_list: List of controller to change
        :param timingobj: Timing Data Object to use
        :param power: True to switch power
        :param ignition: True to switch ignition
        :param startvalue: Whether to start 'on', 'off' or 'tog'
        :param endvalue: Whether to end 'on', 'off' or 'tog'
        :return: True
        """
        root_logger = logging.getLogger('root')
        file_logger = logging.getLogger('file')

        if self.timing.cycles == 0:
            self.timing.display.cyclename = self.timing.switch
            self.changeOutput(self.timing.switchpower, self.timing.switchignition, self.timing.switch)

        if self.timing.startvalue != 'keep':
            file_logger.info("Device states are set to {0} for first run".format(self.timing.startvalue))

            self.changeOutput(True, True, self.timing.startvalue)
            file_logger.info("Device {0} state changed with {1}".format(self.devicename, self.timing.startvalue))
            file_logger.info("Waiting 1s before starting operation")
            time.sleep(1)

        # Run through cycles
        #  if specified (lots of work required, probably will never do)
        # self.root_logger.info("Cycles = {0}".format(self.timing.cycles))
        for i in range(0, self.timing.cycles):
            file_logger.info("Setting up timing for cycle {0}".format(i + 1))
            self.timing.display.cycle = i + 1
            # cycle off time
            # timeon = float(self.timing.timingdata[i][0])
            # timeoff = float(self.timing.timingdata[i][1])
            timeon, timeoff = self.timing.getnext()

            self.changeOutput(self.timing.switchpower, self.timing.switchignition, 'on')

            file_logger.info("Starting Cycle {0} of {1} with timeon={2:03.2f}s and timeoff={3:03.2f}s".format(
                i + 1, self.timing.cycles, timeon, timeoff))

            # time to stay on
            self.timing.display.cyclename = "ON"
            self.timing.display.cycletime = timeon
            time.sleep(timeon)

            # Toggling ports
            self.changeOutput(self.timing.switchpower, self.timing.switchignition, 'tog')

            self.timing.display.cyclename = "OFF"
            self.timing.display.cycletime = timeoff
            time.sleep(timeoff)

        # Setting final state of systems
        if self.timing.endvalue != 'keep':
            self.changeOutput(True, True, self.timing.endvalue)

        return True


class Controller(object):
    def __init__(self, devices_config, remoteip=False, remoteport=5001):
        self.file_logger = logging.getLogger('file')
        self.root_logger = logging.getLogger('root')
        self.__isrunning = False
        self.devices = []
        self.serials = list(set([dev.ikitserial for dev in devices_config]))
        if remoteip and isinstance(remoteip, str):
            self.remoteip = remoteip.encode('utf-8')
        else:
            self.remoteip = remoteip
        self.remoteport = remoteport
        self.interfaces = []
        self.kits = self.get_attached_kits(self.remoteip, self.remoteport)
        if not self.kits:
            raise IOError("Could not find any attached devices")

        self.open_interfaces()

        # generate devices
        for i, dev in enumerate(devices_config):
            for iface, iface_serial in self.interfaces:
                if dev.ikitserial == iface_serial:
                    self.devices.append(Device(dev.name, dev.powerport, dev.powerconnection, dev.ignitionport,
                                               dev.ignitionconnection, dev.timingdata, iface, devconfig=devices_config[i]))

    def run(self):
        self.__isrunning = True
        threads = []
        display_devices = []
        for dev in self.devices:
            t = threading.Thread(target=dev.run_cycles, name=dev.devicename)
            t.daemon = True
            display_devices.append(dev.timing.display)
            t.start()
            threads.append(t)

        display = CountdownConsole(display_devices)
        # display.printCounter()
        display.daemon = True
        display.start()
        try:
            while self.__isrunning:
                alive = False
                for t in threads:
                    alive |= t.isAlive()
                if not alive:
                    self.__isrunning = False
        except KeyboardInterrupt:
            self.root_logger.info("Keyboard Interrupt, Exiting")
        finally:
            display.stop()

    def isrunning(self):
        return self.__isrunning

    @staticmethod
    def get_attached_kits(remoteip, remoteport):
        """
        Get the phidget manager object for identifying phidget devices connected locally or remotely
        :param remoteip: Remtoe IP of Phidget server or non to open local devices
        :param remoteport: Remote port of Phidget server, ignored if opening local devices
        :return: List of [[Device, serial],[...,..]]
        """
        mngr = Manager.Manager()
        if remoteip:
            mngr.openRemoteIP(remoteip, remoteport, password=b'')
        else:
            mngr.openManager()
        time.sleep(2)
        kit_list = mngr.getAttachedDevices()
        # return if no devices found
        if not kit_list:
            return False

        kit_and_serial = []
        for kit in kit_list:
            serial = kit.getSerialNum()
            kit_and_serial.append([kit, serial])
        mngr.closeManager()
        return kit_and_serial

    def open_interfaces(self):
        for serial in self.serials:
            self.interfaces.append([open_InterfaceKit(serial, self.remoteip, self.remoteport), serial])

    def close_interfaces(self):
        for iface, serial in self.interfaces:
            iface.closePhidget()

    def printStates(self):
        """
        Prints the Current State of the ports based on the config of the device
        :return:
        """

        logger = logging.getLogger('file')

        # Requires short wait time for devices to finish current operation
        time.sleep(0.2)

        # Print the heading
        logger.debug("Printing Device States")
        print(
            '\n' + Font.UNDERLINE + "{0:^10} | {1:>8} : {2:^5} : {3:<5} | {4:<6}".format("Device", "Switch", "Port",
                                                                                         "Config",
                                                                                         "State") + Font.END)

        # Prints state of devices
        for dev in self.devices:
            pwrstate, ignstate = dev.getState()
            powerstatusstring = "{0: ^10} | {1: >8} : {2:^5} : {3:^6} | {4: ^14}".format(dev.devicename, "Power",
                                                                                         dev.pwrPort, dev.pwrCon,
                                                                                         pwrstate) + Font.END
            ignitionstatusstring = Font.UNDERLINE + "{0: ^10} | {1: >8} : {2:^5} : {3:^6} | {4: ^14}".format("",
                                                                                                             "Ignition",
                                                                                                             dev.ignPort,
                                                                                                             dev.ignCon,
                                                                                                             ignstate) + Font.END
            print(powerstatusstring)
            print(ignitionstatusstring)
        print('\n')


class SwitchAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(SwitchAction, self).__init__(option_strings, dest, nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        store_true = False
        try:
            if not 't' in values[0] or not 'f' in values[0]:
                store_true = True
            elif len(namespace.device) != len(values[0]) and len(values) != 0 and not isinstance(values, str):
                raise argparse.ArgumentError(self, "Number of options for {0} does not match number of selected devices !".format(option_string))
        except IndexError as e:
            pass

        if len(namespace.device) == 1:
            new_values = [True]
        else:
            new_values = []
            if len(values) == 0 or store_true:
                for i in range(len(namespace.device)):
                    new_values.append(True)
            else:
                for i in values[0]:
                    if i == 'f':
                        new_values.append(False)
                    elif i == 't':
                        new_values.append(True)
                    else:
                        raise ValueError(
                            "{0} is not a valid option for {1}, please choose f/F or t/T".format(i, self.dest))
        setattr(namespace, self.dest, new_values)

class CycleAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(CycleAction, self).__init__(option_strings, dest, nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        if len(namespace.device) != len(values) and len(values) != 1:
            raise argparse.ArgumentError(self, "Number of options for {0} does not match number of selected devices !".format(option_string))
        else:
            new_values = []
            if len(values) == 1:
                for i in range(len(namespace.device)):
                    new_values.append(values[0])
            else:
                if isinstance(values[0], list):
                    values = values[0]
                for i in values:
                    new_values.append(i)
        setattr(namespace, self.dest, new_values)


class PowerStateAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super(PowerStateAction, self).__init__(option_strings, dest, nargs, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        if isinstance(values,str):
            values = [values]
        if len(namespace.device) != len(values) and len(values) != 1:
            raise argparse.ArgumentError(self, "Number of options for {0} does not match number of selected devices !".format(option_string))
        else:
            new_values = []
            if len(values) == 1:
                for i in range(len(namespace.device)):
                    new_values.append(values[0])
            else:
                for i in values:
                    new_values.append(i)
        if option_string == "--switch":
            choices = ['on', 'off', 'tog']
        else:
            choices = ['on', 'off', 'keep']

        for i in new_values:
            if i not in choices:
                raise argparse.ArgumentError(self, "Option {0} not in {1}".format(i, choices))
        setattr(namespace, self.dest, new_values)


def main(args):
    """
        Main parses arguments and controls a connected phidget device.
        Uses a .config file to setup devices with names that can be passed as arguments"""

    # get device configuration file
    config_file_name = 'phidget.config'
    config = getConfig(config_file_name)
    config_devs = config.sections()
    # append all as an option
    config_devs.append('all')
    config_devs =sorted(config_devs)
    # get program config and set remote option defaults
    prg_config_name = 'sysconfig.config'
    sysconfig = getConfig(prg_config_name)
    try:
        configremote_ip = sysconfig.get('remote', 'ip')
    except (NoOptionError, NoSectionError) as e:
        configremote_ip = '127.0.0.1'

    try:
        configremote_port = sysconfig.get('remote', 'port')
    except (NoOptionError, NoSectionError) as e:
        configremote_port = 5001

    # parse arguments
    parser = argparse.ArgumentParser(description='Controls a Phidget device based off a .config file and input options',
                                      formatter_class=RawTextHelpFormatter, usage="%(prog)s {0} [Options]".format("|".join(config_devs)))
    parser.add_argument('device', nargs='+', choices=config_devs, type=str.lower,
                        help='Device Options to use based off .config file')
    parser.add_argument('-i', '--ignition', nargs='*', default=False, action=SwitchAction, type=str.lower,
                        help='Select Ignition to toggle')
    parser.add_argument('-p', '--power', nargs='*', default=False, action=SwitchAction, type=str.lower,
                        help='Select Power to toggle')
    parser.add_argument('-s', '--switch', default=False, choices=['on', 'off', 'tog'], action=PowerStateAction,
                        help='Turn on, off or toggle the switch')
    parser.add_argument('-c', '--cycles', nargs='*', type=int, default=0, action=CycleAction,
                        help='Number of cycles-defult 1 if not supplied')
    parser.add_argument('-ton', '--timeon', nargs='*', type=float, action=CycleAction, default=False,
                        help='Time on in seconds - Default 1 if not specified')
    parser.add_argument('-toff', '--timeoff', nargs='*', type=float, default=False, action=CycleAction,
                        help='Time off in seconds - default 0 if not specified')
    parser.add_argument('--script', nargs='*', action=CycleAction, help='path to script file, script file must \
            contain ontime then offtime as a comma separated list i.e \n10,2\n5,6\netc...')
    parser.add_argument('--clog', default='INFO', type=str.upper,
                        choices=['INFO', 'WARNING', 'ERROR', 'CRITICAL', 'DEBUG'],
                        help='Select logging Level, Default is all info, errors and warnings only')
    parser.add_argument('--rlog', default='DEBUG', type=str.upper,
                        choices=['INFO', 'WARNING', 'ERROR', 'CRITICAL', 'DEBUG'],
                        help='Select logging Level, Default is all info, errors and warnings only')
    parser.add_argument('--flog', default='INFO', type=str.upper,
                        choices=['INFO', 'WARNING', 'ERROR', 'CRITICAL', 'DEBUG'],
                        help='Select logging Level, Default is all info, errors and warnings only')
    parser.add_argument('--debug', default=False, action='store_const', const=True, help='Debug Output')
    parser.add_argument('-P', '--printstate', default=False, action='store_true',
                        help='Prints the current state devices selected')
    parser.add_argument('--start', nargs='*', default=False, action=PowerStateAction,
                        help='The first start state of the systems')
    parser.add_argument('--end', nargs='*',default=False, action=PowerStateAction,
                        help='The final state for the systems')
    parser.add_argument('-R', '--remoteip', default=False, const=configremote_ip, type=str, nargs='?',
                        help='Remote ip of phidgets webservice')
    parser.add_argument('-RP', '--remoteport', default=configremote_port, const=configremote_port, type=int, nargs='?',
                        help="Remote port of phidgets webservice")
    try:
        args = parser.parse_args(args)
    except (ValueError, TypeError) as e:
        print("{0}".format(e))
        parser.print_help()
        exit(2)

    # expand options not parsed base on number of devices to create defaults
    if not args.ignition:
        args.ignition = []
        for i in range(len(args.device)):
            args.ignition.append(False)
    if not args.power:
        args.power = []
        for i in range(len(args.device)):
            args.power.append(False)

    if not args.switch:
        args.switch = []
        for i in range(len(args.device)):
            args.switch.append('tog')

    for i in range(len(args.ignition)):
        if not args.ignition[i] and not args.power[i]:
            print("{0} device have no ignition or power to switch".format(args.device[i]))
            parser.print_help()
            exit(2)

    if args.cycles:
        if not args.timeoff:
            args.timeoff = []
            for i in range(len(args.device)):
                args.timeoff.append(1.0)
        if not args.timeon:
            args.timeon= []
            for i in range(len(args.device)):
                args.timeoff.append(1.0)

    configure_logging(args.rlog, args.flog, args.clog, args.debug, args.cycles)

    if args.cycles:
        file_logger = logging.getLogger('datefile')
    else:
        file_logger = logging.getLogger('file')

    root_logger = logging.getLogger('root')
    # debug_logger = logging.getLogger('debug')
    console_logger = logging.getLogger('console')
    if args.debug:
        console_logger.info("Debug logging enabled...")

    # get config devices
    config_devices = []

    if 'all' in args.device:
        sections = config.sections()
        position = 0
        for s in sections:
            if not config.has_option(s, 'keys'):
                config_devices.append(s)
                args.ignition.insert(position, args.ignition[position])
                args.power.insert(position, args.power[position])
                if args.cycles:
                    args.cycles.insert(position, args.cycles[position])
                if args.switch:
                    args.switch.insert(position, args.switch[position])
                if args.script:
                    args.script.insert(position, args.script[position])
                position += 1
    else:
        position = 0
        for key in args.device:
            if config.has_option(key, 'keys'):
                keys = config.get(key, 'keys').split(',')
                for k in keys:
                    if k not in config_devices:
                        args.ignition.insert(position, args.ignition[position])
                        args.power.insert(position, args.power[position])
                        args.switch.insert(position, args.switch[position])
                        if args.cycles:
                            args.cycles.insert(position, args.cycles[position])

                        if args.script:
                            args.script.insert(position, args.script[position])
                        config_devices.append(k)
                    position += 1
            else:
                if key not in config_devices:
                    config_devices.append(key)
                    position += 1

    if args.cycles or args.script:
        if not args.end:
            args.end = []
            for i in range(len(args.device)):
                args.end.append('keep')
        if not args.start:
            args.start = []
            for i in range(len(args.device)):
                args.start.append('keep')

    dev_count = len(config_devices)
    if args.script:
        timingobjs = []
        for i in range(dev_count):
            timingobjs.append(load_script(args.script[i]))
    elif args.cycles >= 1:
        timingobjs = calc_cycles(args.cycles, args.timeon, args.timeoff, args.device)
    else:
        timingobjs = []
        for i in range(dev_count):
            timingobjs.append(TimingData(switch=args.switch[i]))

    for i, timingobj in enumerate(timingobjs):
        timingobj.switchpower = args.power[i]
        timingobj.switchignition = args.ignition[i]
        if args.cycles or args.script:
            timingobj.startvalue = args.start[i]
            timingobj.endvalue = args.end[i]

    dev_configs = create_device_configurations(config_file_name, config_devices, timingobjs)

    try:
        con = Controller(dev_configs, args.remoteip, args.remoteport)
    except IOError as e:
        root_logger.error("{0}".format(e))
        exit(1)

    # If print option, print state data for devices and exit
    if args.printstate:
        con.printStates()
        con.close_interfaces()
        exit(0)
    try:
        con.run()
        while con.isrunning():
            time.sleep(0.1)
    except KeyboardInterrupt:
        pass

    con.printStates()
    con.close_interfaces()


if __name__ == "__main__":
    main(sys.argv[1:])
