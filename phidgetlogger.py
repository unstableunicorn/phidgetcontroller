import logging
import sys
import os
from datetime import datetime
import time

# python 3 and 2 compatibility import and global variables
if (sys.version_info > (3, 0) or sys.version < (2, 7)):
    # Python 3 imports  in this block
    import io as StringIO
else:
    # Python 2 imports in this block
    import StringIO


class DateFileHandler(logging.FileHandler, object):
    """
    Default file handler for logging to files
    """
    def __init__(self, logdir='logs', filename='runlog.log', mode='a', delay=True, date=False):
        """
        Initialisation of Date file handler
        :param logdir: Directory to log to
        :param filename: File name to log to
        :param mode: File access mode, Append by default
        :param delay: Whether to delay creation of file, if nothing logged to file then file not created
        :param date: True to prepend date to log file name
        """
        if date:
            log_file_name = "{0}_{1}".format(datetime.now().strftime("%Y_%m_%d_%H_%M_%S"), filename)
        else:
            log_file_name = filename

        log_file_path = os.path.join(logdir, log_file_name)
        if not os.path.exists(logdir):
            os.makedirs(logdir)
        super(DateFileHandler, self).__init__(log_file_path, mode, delay=delay)


class UTCFormatter(logging.Formatter, object):
    """
    Logging formatter to use UTC time instead of local time
    """
    converter = time.gmtime


class StringIOStream(logging.StreamHandler, object):
    def __init__(self):
        #for python 2.6 support the keyword was different
        if sys.version < (2, 7):
            super(StringIOStream, self).__init__(strm=StringIO.StringIO())
        else:
            super(StringIOStream, self).__init__(stream=StringIO.StringIO())


class Logger(object):
    def __init__(self, name, loglevel, filename=None, logdir='logs',
                 logformat="%(asctime)s: %(name)s: %(levelname)s: %(message)s",
                 dateformat='%d/%m/%Y %H:%M:%S UTC'):

        # if no file name use default
        if filename is None:
            log_file_name = datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '_runlog.log'
        else:
            log_file_name = filename
        log_file_path = os.path.join(logdir, log_file_name)

        if not os.path.exists(logdir):
            os.makedirs(logdir)
        log_level = logging.getLevelName(loglevel)
        log_formatter = logging.Formatter(fmt=logformat,
                                          datefmt=dateformat)
        log_formatter.converter = time.gmtime

        # Create loggers
        self.file_console = logging.getLogger(name + "(root)")
        self.console = logging.getLogger(name + "(console)")
        self.file = logging.getLogger(name + "(file)")

        # Add file handling
        log_file_handler = logging.FileHandler(log_file_path)
        log_file_handler.setFormatter(log_formatter)
        self.file_console.addHandler(log_file_handler)
        self.file.addHandler(log_file_handler)

        # Add console handling
        log_console_handler = logging.StreamHandler()
        log_console_handler.setFormatter(log_formatter)
        self.file_console.addHandler(log_console_handler)
        self.console.addHandler(log_console_handler)

        # set levels
        self.file_console.setLevel(log_level)
        self.file.setLevel(log_level)
        self.console.setLevel(log_level)
