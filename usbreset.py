"""
    Example code for resetting the USB port that a Teensy microcontroller is
    attached to. There are a lot of situations where a Teensy or Arduino can
    end up in a bad state and need resetting, this code is useful for 
"""

import os
import platform

try:
    import fcntl
except ImportError:
    print("Warning, {0} is not supported on this plateform".format(__name__))
    pass
import subprocess
import logging


class Error(Exception):
    """base class for exceptions in this module"""
    pass


class InvalidDeviceError(Error):
    """ Exception raised if device is not found or invalid"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class OSSupportError(Error):
    """Exception raised if OS related error"""

    def __init__(self, msg):
        """
        Initialisation
        :param msg: String with message of error
        """
        self.msg = msg
        self.osplatform = platform.system()
        self.osname = os.name
        # def __str__(self):
        #     return ("{0} OS Name: {1} OS Platform: {2}".format(self.msg, self.osname, self.osplatform))


# Equivalent of the _IO('U', 20) constant in the linux kernel.
class UsbReset(object):
    def __init__(self, usbdevice):
        """
        Initialisation
        :param usbdevice: String of usb device to look for
        """
        self.USBDEVFS_RESET = ord('U') << (4 * 2) | 20
        self.usbdevice = usbdevice
        self.logger = logging.getLogger("root")

    def getusbdevpath(self):
        """
            Gets the devfs path to a usb device by scraping the output
            of the lsusb command

            The lsusb command outputs a list of USB devices attached to a computer
            in the format:
                Bus <bus number> Device <device number>: ID <device id> <Company device name>
            The devfs path to these devices is:
                /dev/bus/usb/<busnum>/<devnum>
            This function generates that path.
        :return: /dev/bus/usb/<busnum>/<devnum>
        """

        if 'linux' in platform.system().lower():
            proc = subprocess.Popen(['lsusb'], stdout=subprocess.PIPE)
            out = str(proc.communicate()[0])
            lines = out.split('\n')
            paths = []
            for line in lines:
                if self.usbdevice in line:
                    parts = line.split()
                    bus = parts[1]
                    dev = parts[3][:3]
                    paths.append('/dev/bus/usb/%s/%s' % (bus, dev))

            if len(paths) > 0:
                return paths

            self.logger.error("No Device named {0} found".format(self.usbdevice))
            raise InvalidDeviceError('No Device with that name')
        else:
            self.logger.error("OS Not supported!")
            raise OSSupportError("OS Not Supported: ")

    def sendreset(self, dev_path):
        """
        ends the USBDEVFS_RESET IOCTL to a USB device.

        :param dev_path: The devfs path to the USB device (under /dev/bus/usb/)
        :return: None
        """
        self.logger.info("resetting Device on path {0}".format(dev_path))
        fd = os.open(dev_path, os.O_WRONLY)
        try:
            fcntl.ioctl(fd, self.USBDEVFS_RESET, 0)
        finally:
            os.close(fd)

    def resetusbdev(self):
        """
        Finds a usb device and reset it.
        :return: True if Success, False if Fail
        """
        try:
            paths = self.getusbdevpath()
        except InvalidDeviceError as e:
            self.logger.error("{0}".format(e))
            return False

        for path in paths:
            self.sendreset(path)

        return True
