import sys
import platform
import logging

from controller import get_phidget_manager, configure_logging
from usbreset import *

if sys.version_info < (2, 7):
    import unittest2 as unittest
else:
    import unittest

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
# set up logger to test logs
logfile = os.path.join(THIS_DIR, 'testlogger.config')
configure_logging(date=True, log_config=logfile)


class TestUsbReset(unittest.TestCase):
    def setUp(self):
        self.logger = logging.getLogger('file')
        self.stream = logging.getLogger('console').handlers[0].stream
        devicelist, self.mngr = get_phidget_manager()
        self.os_is_windows = False if 'linux' in platform.system().lower() else True

        if not devicelist:
            self.mngr.closeManager()
            raise self.skipTest("Skipping: No Local Phidget Devices Attached to test")
        self.obj = UsbReset('Phidget')

    def test_getusbdevpath(self):
        if self.os_is_windows:
            with self.assertRaises(OSSupportError):
                self.obj.getusbdevpath()
        else:
            self.assertNotEqual(self.obj.getusbdevpath(), False)

    def test_resetusbdev(self):
        if self.os_is_windows:
            self.skipTest("Skipping test_sendreset: Not supported on this platform")

        self.assertTrue(self.obj.resetusbdev())
        print('Stream = [{0}]'.format(self.stream.getvalue()))

    def tearDown(self):
        self.mngr.closeManager()


if __name__ == '__main__':
    unittest.main()
